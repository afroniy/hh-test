from random import randint
import sys, getopt
import random

def char_range(c1, c2):
    for c in xrange(ord(c1), ord(c2)+1):
        yield chr(c)

digits = list(char_range('0','9')) + list(char_range('a','z'))
numbers = [i for i in range(0,len(digits)) ]

DIGITS_DICT = dict(zip(numbers, digits))


MAX_DIGITS = 1000
MAX_SMALL_PERIOD = 6 


def factorize(n):
    divisors = [ d for d in range(2,n//2+1) if n % d == 0 ] 
    def is_prime(d): return all( d % od != 0 for od in divisors if od != d )
    prime_divisors = [ d for d in divisors if is_prime(d) ]
    return prime_divisors


def int_part_convert(a,b,p):
    n = a/b
    digit = ''
    number = ''
    while n>0:
        d = n % p
        n = n/p
        number = DIGITS_DICT[d] + number
    if not number: number = 0
    return number


def float_part_convert(a,b,p):
    n_int = a/b 
    n = a-n_int*b
    i = 0 
    number = ''
    (prepend_length, period_length) = get_period(b, p) 
    periodic = prepend_length + period_length

    while n and ((i < prepend_length+period_length) or not periodic): 
        n = (n % b)
        n = n*p
        digit_next = DIGITS_DICT[n/b]
        number += digit_next
        i += 1 
        # try to extract small periods
        if i > prepend_length and (i - prepend_length) < MAX_SMALL_PERIOD*2:
             tail = number[prepend_length+1:]
             l = len(tail) 
             #print "tail: %s" % tail
             #print "%s %s" % (tail[:l/2], tail[l/2:])
             if l % 2 == 0 and tail[:l/2] == tail[l/2:]:
                 if set(tail) != set('0',) and set(tail): 
                     number = number[:-(l/2+1)]
                     break
    # beautify results
    if len(set(number[prepend_length:])) == 1:
        repeated =  number[prepend_length] 
        number = number[:prepend_length] + repeated
        #number = number[:-2]
    while number[-1] == '0': number = number[:-1]

    if periodic and number:
        number = number[:prepend_length] + '(' + number[prepend_length:] 
        number += ')' 
    if not number: number = '0'
    return number


def gcd(a, b):
    while b:
        a, b = b, a%b
    return a

def gcd_m(*args):
    mpxs = list(args) 
    result = mpxs.pop()
    for m in mpxs: 
        result = gcd(result, m)
    return result

def get_mpx_power(a,m):
    result = 0
    while a % m == 0: 
        result += 1 
        a = a/m
    return result


def lcm(*args):
    p = reduce(lambda x, y: x*y, args)
    return abs(p)/gcd_m(*args)


def get_prepend_lenght(factors_pb,b,p):
    powers = []
    if not factors_pb:
        return 0
    for m in factors_pb:
        m_pow = get_mpx_power(b,m)
        powers.append(m_pow)
    return max(powers)


def carmichael(n):
    if n == 1 or n == 2: return 1
    if n == 4: return 2
    factors = factorize(n)    
    if not factors:
        factors.append(n)
    if len(factors) == 1:
        m = factors.pop()
        if m == 2:
            k = get_mpx_power(n,m) 
            return pow(2,k-2)
        else:
            k = get_mpx_power(n,m)
            return pow(m,k-1)*(m-1)
    else:
       sub_tasks = [carmichael(m*get_mpx_power(n,m)) for m in factors]
       return lcm(*sub_tasks)

def get_period(b,p):
    factors_p = set(factorize(p))
    factors_b = set(factorize(b))
    if not factors_p:
        factors_p.add(p)
    if not factors_b:
        factors_b.add(b)

    if factors_b.issubset(factors_p):
        return (0,0)

    factors_pb = factors_b.intersection(factors_p)
    factors_ob = factors_b.difference(factors_p)

    prepend_lenght = get_prepend_lenght(factors_pb,b,p)
    period_length = carmichael(b)
    return prepend_lenght, period_length

def convert_fraction(a, b, p):
    int_part = int_part_convert(a, b, p) 
    float_part = float_part_convert(a, b, p)
    return "%s.%s" % (int_part, float_part)



def load_data(filename):
    with open(filename, "r") as ins:
        data = []
        for line in ins:
            try:
                (a, b, p) = [ int(a) for a in line.split(" ") ]
                if  1< p <= 29:
                    data.append((a,b,p))
                else: raise ValueError
            except ValueError as e:
                print "Wrong file format: %s" % e
                sys.exit(2)
        return data 

def main(argv):
   def print_help():
       print """
Convert fractions in a digital expression in the specified scale of notation
       p_fractions.py -i <inputfile>  
       """
       sys.exit(2)

   inputfile = ''
   try:
      opts, args = getopt.getopt(argv,"i:",["ifile="])
   except getopt.GetoptError:
      print_help() 
   if not opts: print_help()
   for opt, arg in opts:
       if opt in ("-i", "--ifile"):
          inputfile = arg
   data = load_data(inputfile)

   for line in data:
       print convert_fraction(*line)


if __name__ == '__main__':
    main(sys.argv[1:])
