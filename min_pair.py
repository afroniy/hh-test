import timeit
import math
from random import randint
import datetime

import sys, getopt

MAX_X = 1000
MAX_Y = 1000

def populate(n):
    return [(randint(0,MAX_X), randint(0,MAX_Y)) for i in range(n)]


def metric(d1, d2):
    return math.sqrt(pow(d1[0]-d2[0], 2) + pow(d1[1]-d2[1],2))

def cheap_metric(d1, d2):
     return max(abs(d1[0]-d1[1]), abs(d2[1]-d2[1]))

def median(S):
    overall = sum([p[0] for p in S])
    return float(overall)/len(S)

def split_set(S, m):
    S1, S2 = set(), set()
    for p in S:
        if p[0]>m: S2.add(p)
        else: S1.add(p)
    return (S1, S2)

def delta_stripe(S, d, m):
    P1, P2 = set(), set()
    for p in S:
        if abs(p[0] - m) < d:
           if p[0]>m: P2.add(p)
           else: P1.add(p)
    return (P1, P2)
 

def get_full_mesh(S1,S2):
    S = S1.union(S2)
    try:
       min_distance =  abs(metric(S.pop(), S.pop()))
    except KeyError:
       min_distance = float('inf') 
    for p1 in S1:
	    for p2 in S2:
                distance = metric(p1, p2)
                if min_distance > distance > 0: min_distance = distance
    return min_distance

def min_distance(S):
    if len(S) == 2:
        return abs(metric(S.pop(), S.pop())) 
    elif len(S) == 1:
        return float('inf')
    else:
        m = median(S)
        S1, S2 = split_set(S, m)
        if S == S1 or S == S2:
            return get_full_mesh(S,S)
        ds = min(min_distance(S1), min_distance(S2)) 
        P1, P2 = delta_stripe(S, ds, m)
        dl = get_full_mesh(P1, P2)
        d = min(ds, dl) 
        return d 

def load_data(filename):
    with open(filename, "r") as ins:
        dots = []
        for line in ins:
            try:
               (x, y) = [ int(a) for a in line.split(" ") ]
            except ValueError as e:
               print "Wrong file format: %s" % e.message
               sys.exit(2)
            dots.append((x,y))
        return dots 

def main(argv):
   def print_help():
       print """
Find a minimal distance between points on the plane
       min_distance.py -i <inputfile>  
       """

   inputfile = ''
   try:
      opts, args = getopt.getopt(argv,"hi:",["ifile="])
   except getopt.GetoptError:
      print 'min_distance.py -i <inputfile> '
      sys.exit(2)
   if not opts: print_help()
   for opt, arg in opts:
       if opt in ("-i", "--ifile"):
          inputfile = arg
   dots = load_data(inputfile)
   #dots = populate(100)
   print "Minimal distance: %s" % min_distance(dots)
    

if __name__ == '__main__':
    main(sys.argv[1:])

